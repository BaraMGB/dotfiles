#!/bin/bash

apt update
apt install -y wget curl
apt install -y neovim

apt install -y nodejs ccls clang

mkdir -p ~/.config
mkdir -p ~/.config/nvim

### install vim plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
###


cp /dotfiles/.bashrc ~/.bashrc
cp /dotfiles/init.vim ~/.config/nvim/init.vim




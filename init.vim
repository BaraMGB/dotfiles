:set number
:set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
":set list
:set expandtab
:set lcs+=space:·
:set splitbelow splitright
:set nohlsearch

set cursorline
au InsertEnter * hi CursorLine ctermbg=52
au InsertLeave * hi CursorLine ctermbg=234


call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'preservim/nerdtree'

call plug#end()

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif
let NERDTreeCustomOpenArgs={'file':{'where': 't'}}
nnoremap <C-n> :NERDTreeToggle %<CR>
nnoremap <C-f> :NERDTreeFind<CR>
nnoremap <F8> :bprevious<CR>
nnoremap <F9> :bnext<CR>
nnoremap <F5> :buffers<CR>:buffer<Space>
nnoremap <F1> :e ~/.config/nvim/init.vim<CR>
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <F4> :tabnew <CR> 
nnoremap <F11> :tab split<CR>
nnoremap Q !!$SHELL<CR>
nnoremap <C-Q> :wa<CR> :mksession! ~/.config/nvim/ide_session.vim <CR> :qa <CR>
lua <<EOF

require'nvim-treesitter.configs'.setup {
  ensure_installed = { "c", "cpp", "lua", "rust" },
  sync_install = false,
  ignore_install = { "javascript" },
  highlight = {
	enable = true, disable = { "" }, additional_vim_regex_highlighting = true,
	},
  }
EOF

"---------------------------------------------------coc setup-----------------
"-----------------------------------------------------------------------------
" if hidden is not set, TextEdit might fail. set hidden Some servers have issues with backup files, see #649 set nobackup set nowritebackup Better display for messages set cmdheight=2 You will have bad experience for diagnostic messages when it's default 4000. set updatetime=300 don't give |ins-completion-menu| messages. set shortmess+=c always show signcolumns set signcolumn=yes Use tab for trigger completion with characters ahead and navigate. Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin. 
inoremap <silent><expr> <TAB> 
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Use `[c` and `]c` to navigate diagnostics
nmap <silent> gb <Plug>(coc-diagnostic-prev)
nmap <silent> gn <Plug>(coc-diagnostic-next)
" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction
" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')
" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)
" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end
" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)
" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)
" Use <tab> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <S-TAB> <Plug>(coc-range-select-backword)
" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')
" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)
" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')
" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

"-----------------------------------------------------------------------------
"                      Color theme
"-----------------------------------------------------------------------------
	
highlight Normal           ctermfg=7    ctermbg=none    cterm=none 
highlight LineNr           ctermfg=96    ctermbg=none    cterm=none 
highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none 
highlight Space            ctermfg=8    ctermbg=none    cterm=italic

"highlight VertSplit        ctermfg=0    ctermbg=8       cterm=none
highlight Statement        ctermfg=3    ctermbg=none    cterm=none 
"highlight Directory        ctermfg=4    ctermbg=none    cterm=none
"highlight StatusLine       ctermfg=7    ctermbg=8       cterm=none
"highlight StatusLineNC     ctermfg=7    ctermbg=8       cterm=none
"highlight NERDTreeClosable ctermfg=2 
"highlight NERDTreeOpenable ctermfg=8
highlight Comment          ctermfg=8    ctermbg=none    cterm=italic
"highlight Constant         ctermfg=12   ctermbg=none    cterm=none
"highlight Special          ctermfg=4    ctermbg=none    cterm=none
highlight Identifier       ctermfg=34    ctermbg=none    cterm=none
"highlight PreProc          ctermfg=5    ctermbg=none    cterm=none 
highlight String           ctermfg=13   ctermbg=none    cterm=none
highlight Number           ctermfg=63    ctermbg=none    cterm=none
"highlight Function         ctermfg=11    ctermbg=none    cterm=none 
"highlight Type             ctermfg=168    ctermbg=none    cterm=none 
" highlight WildMenu         ctermfg=0       ctermbg=80      cterm=none
hi TSField		ctermfg=34		ctermbg=none    cterm=none 
hi TSNamespace  ctermfg=162     ctermbg=none    cterm=none 
hi TSKeyword    ctermfg=15     ctermbg=none    cterm=none 
hi TSClass    ctermfg=160     ctermbg=none    cterm=none 
hi TSFunction    ctermfg=228     ctermbg=none    cterm=none 
hi TSType       ctermfg=166     ctermbg=none    cterm=none 
hi CocSemClass       ctermfg=160     ctermbg=none    cterm=none 
"hi TSSymbol    ctermfg=10     ctermbg=none    cterm=none 
hi TSConditional  ctermfg=172     ctermbg=none    cterm=none 
hi TSKeywordOperator      ctermfg=1     ctermbg=none    cterm=none 
hi TSTypePrimitive    ctermfg=160     ctermbg=none    cterm=none 
hi  TSTypeBuiltin   ctermfg=160     ctermbg=none    cterm=none 
hi TSVariable    ctermfg=33     ctermbg=none    cterm=none 
 highlight Folded           ctermfg=103     ctermbg=234     cterm=none 
 highlight FoldColumn       ctermfg=103     ctermbg=234     cterm=none 
 highlight DiffAdd          ctermfg=none    ctermbg=23      cterm=none 
 highlight DiffChange       ctermfg=none    ctermbg=56      cterm=none 
 highlight DiffDelete       ctermfg=168     ctermbg=96      cterm=none 
 highlight DiffText         ctermfg=0       ctermbg=80      cterm=none 
 highlight SignColumn       ctermfg=244     ctermbg=235     cterm=none 
 highlight Conceal          ctermfg=251     ctermbg=none    cterm=none
 highlight SpellBad         ctermfg=168     ctermbg=none    cterm=underline
 highlight SpellCap         ctermfg=80      ctermbg=none    cterm=underline
 highlight SpellRare        ctermfg=121     ctermbg=none    cterm=underline
 highlight SpellLocal       ctermfg=186     ctermbg=none    cterm=underline 
 highlight Pmenu            ctermfg=251     ctermbg=234     cterm=none
 highlight PmenuSel         ctermfg=0       ctermbg=111     cterm=none
 highlight PmenuSbar        ctermfg=206     ctermbg=235     cterm=none
 highlight PmenuThumb       ctermfg=235     ctermbg=206     cterm=none
 highlight TabLine          ctermfg=244     ctermbg=234     cterm=none
 highlight TablineSel       ctermfg=0       ctermbg=247     cterm=none
 highlight TablineFill      ctermfg=244     ctermbg=234     cterm=none
 highlight CursorColumn     ctermfg=none    ctermbg=236     cterm=none
 highlight CursorLine       ctermfg=none    ctermbg=236     cterm=none
 highlight ColorColumn      ctermfg=none    ctermbg=236     cterm=none
 highlight Cursor           ctermfg=0       ctermbg=5       cterm=none
 highlight htmlEndTag       ctermfg=114     ctermbg=none    cterm=none
 highlight xmlEndTag        ctermfg=114     ctermbg=none    cterm=none
"
"----------------------------------------------------------------------------
"        Status Line
"----------------------------------------------------------------------------
set statusline=
set statusline+=%#DiffAdd#%{(mode()=='n')?'\ \ NORMAL\ ':''}
set statusline+=%#DiffChange#%{(mode()=='i')?'\ \ INSERT\ ':''}
set statusline+=%#DiffDelete#%{(mode()=='r')?'\ \ RPLACE\ ':''}
set statusline+=%#Cursor#%{(mode()=='v')?'\ \ VISUAL\ ':''}
set statusline+=\ %n\           " buffer number
set statusline+=%#Visual#       " colour
set statusline+=%{&paste?'\ PASTE\ ':''}
set statusline+=%{&spell?'\ SPELL\ ':''}
set statusline+=%#CursorIM#     " colour
set statusline+=%R                        " readonly flag
set statusline+=%M                        " modified [+] flag
set statusline+=%#Cursor#               " colour
set statusline+=%#CursorLine#     " colour
set statusline+=\ %t\                   " short file name
set statusline+=%=                          " right align
set statusline+=%#CursorLine#   " colour
set statusline+=\ %Y\                   " file type
set statusline+=%#CursorIM#     " colour
set statusline+=\ %3l:%-2c\         " line + column
set statusline+=%#Cursor#       " colour
set statusline+=\ %3p%%\                " percentage
